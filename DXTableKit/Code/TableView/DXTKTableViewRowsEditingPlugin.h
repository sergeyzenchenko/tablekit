//
//  DXTKTableViewRowsEditingInstructor.h
//  DXTableKit
//
//  Created by Vladimir Shevchenko on 5/27/14.
//  Copyright (c) 2014 111min. All rights reserved.
//

@protocol DXTKTableViewRowsEditingPlugin <NSObject>

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
                                  object:(id)object;

- (NSString *)tableView:(UITableView *)tableView
titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
                 object:(id)object;

- (BOOL)tableView:(UITableView *)tableView
shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
           object:(id)object;

- (void)tableView:(UITableView *)tableView
willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
           object:(id)object;

- (void)tableView:(UITableView *)tableView
didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
           object:(id)object;

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
           object:(id)object;


@end